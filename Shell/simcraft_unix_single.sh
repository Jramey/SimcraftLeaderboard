#!/bin/bash
source ./settings.sh
PLAYER="$1"
echo "lock" >/var/www/cotd/forums/simc.lock
java -jar /simstuff/SimcraftBatch.jar "$GUILD" "$REALM" wwwhome="$WWW_PATH" simcdir="/simstuff" page="$PURGE_URL" setRegion="$REGION" dumpAvatars="true" avatarOutput="$AVATAR_PATH" dumpilvl="true" ilvldir="$WWW_PATH/ilvl" custom_sim="$PLAYER" doWeights="false"
chmod a+x /simstuff/runSimulations.sh
/simstuff/runSimulations.sh
page=$(curl $PAGE_URL)
rm $WWW_PATH/simc.lock
rm $SIM_PATH/runSimulations.sh
