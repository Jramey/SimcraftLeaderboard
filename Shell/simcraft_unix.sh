#!/bin/bash
source ./settings.sh
echo "lock" >$WWW_PATH/simc.lock
java -jar $SIM_PATH/SimcraftBatch.jar "$GUILD" "$REALM" wwwhome="$WWW_PATH" simcdir="/simstuff" page="$PURGE_URL" setRegion="$REGION" dumpAvatars="true" avatarOutput="$AVATAR_PATH" dumpilvl="true" ilvldir="$WWW_PATH/ilvl"
chmod a+x $SIM_PATH/runSimulations.sh
$SIM_PATH/runSimulations.sh
page=$(curl $PAGE_URL)
rm $WWW_PATH/simc.lock
rm $SIM_PATH/runSimulations.sh
