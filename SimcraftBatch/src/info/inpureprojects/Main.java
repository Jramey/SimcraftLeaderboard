package info.inpureprojects;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.halverson.wowapi.dao.CharacterDao;
import org.halverson.wowapi.dao.GuildDao;
import org.halverson.wowapi.entity.Character;
import org.halverson.wowapi.entity.*;
import org.halverson.wowapi.exception.CharacterNotFoundException;
import org.halverson.wowapi.exception.WowApiException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.*;
import java.net.URL;
import java.util.*;

public class Main {

    public static final int MAX_LEVEL = 100;
    public static final String command = "%s/simc/engine/simc armory=%s,%s,%s %s skill=%s enemy=Steve tmi_boss=T17N html=/var/www/cotd/forums/simulations/%s_%s.html";
    public static final File save = new File("cache.json");
    public static Main instance;
    public static int wait = 0;
    private String realm = "Hyjal";
    private String guildName = "Children of the Ðamned";
    private boolean doWeights = false;
    private boolean dumpAvatars = false;
    private boolean dosim = true;
    private HashMap<String, String> last = new HashMap<String, String>();
    private Gson json = new GsonBuilder().setPrettyPrinting().create();
    private String avatarOutput = "avatars";
    private boolean dumpilvl = false;
    private String ilvldir = "ilvl";
    private ArrayList<String> bannedPlayers = new ArrayList();
    private boolean debug = false;
    private File simfile;
    private boolean custom_sim = false;
    private String custom_name = "";
    private boolean dumpClass = false;
    private String classDir = "classes";
    private Region region = Region.US;
    public static final String base = "http://us.battle.net/static-render/%s/";
    public static String base_region;
    private String simc_dir;
    private String www_home;
    private String page;
    private File simLog;

    public Main(String[] args) {
        try {
            if (args.length > 0) {
                this.guildName = args[0].replace("\"", "");
                this.realm = args[1].replace("\"", "");
                for (String s : args) {
                    String option = s.split("=")[0].replace("\"", "");
                    if (option.equalsIgnoreCase("doweights")) {
                        String b = s.split("=")[1];
                        if (b.equals("true")) {
                            doWeights = true;
                        }
                        print("doWeights=%s", this.doWeights);
                    }
                    if (option.equalsIgnoreCase("dumpavatars")) {
                        String b = s.split("=")[1];
                        if (b.equals("true")) {
                            dumpAvatars = true;
                        }
                    }
                    if (option.equalsIgnoreCase("nosim")) {
                        String b = s.split("=")[1];
                        if (b.equals("true")) {
                            dosim = true;
                        } else {
                            dosim = false;
                        }
                    }
                    if (option.equalsIgnoreCase("avataroutput")){
                        String b = s.split("=")[1];
                        new File(avatarOutput = b).mkdirs();
                    }
                    if (option.equalsIgnoreCase("dumpilvl")){
                        String b = s.split("=")[1];
                        if (b.equals("true")) {
                            dumpilvl = true;
                        } else {
                            dumpilvl = false;
                        }
                    }
                    if (option.equalsIgnoreCase("ilvldir")){
                        String b = s.split("=")[1];
                        new File(ilvldir = b).mkdirs();
                    }
                    if (option.equalsIgnoreCase("banned")){
                        String b = s.split("=")[1];
                        String parse[] = b.split(",");
                        bannedPlayers.addAll(Arrays.asList(parse));
                    }
                    if (option.equalsIgnoreCase("custom_sim")){
                        String b = s.split("=")[1];
                        this.custom_sim = true;
                        this.custom_name = String.format("%s", b);
                    }
                    if (option.equalsIgnoreCase("dumpclass")){
                        String b = s.split("=")[1];
                        if (b.equals("true")) {
                            dumpClass = true;
                        } else {
                            dumpClass = false;
                        }
                    }
                    if (option.equalsIgnoreCase("classdir")){
                        String b = s.split("=")[1];
                        new File(classDir = b).mkdirs();
                    }
                    if (option.equalsIgnoreCase("setregion")){
                        String b = s.split("=")[1];
                        for (Region r : Region.values()){
                            if (b.equals(r.toString())){
                                this.region = r;
                            }
                        }
                    }
                    if (option.equalsIgnoreCase("simcdir")){
                        String b = s.split("=")[1];
                        new File(simc_dir = b);
                        simfile = new File(String.format("%s/runSimulations.sh", simc_dir));
                        simLog = new File(String.format("%s/simlog.log", simc_dir));
                    }
                    if (option.equalsIgnoreCase("wwwhome")){
                        String b = s.split("=")[1];
                        new File(www_home = b);
                    }
                    if (option.equalsIgnoreCase("page")){
                        String b[] = s.split("=");
                        String t = "";
                        t+=b[1] + "=" + b[2];
                        page = t;
                    }
                }
            }
        } catch (Throwable t) {
        }
        TreeMap<Integer, String> simcraft_versions = new TreeMap();
        try{
            print("Connecting to %s", new URL("http://www.simulationcraft.org").toString());
            Document doc = Jsoup.connect(new URL("http://www.simulationcraft.org/download.html").toString()).userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").referrer("http://www.google.com").get();
            Elements links = doc.select("a[href]");
            for (Element e : links){
                if (e.attr("abs:href").contains("release-")){
                    String[] parse = e.attr("abs:href").split("-");
                    String v = parse[parse.length - 1].replace(".html", "");
                    if (v.matches("^[0-9]+$")){
                        simcraft_versions.put(Integer.valueOf(v), e.attr("abs:href"));
                    }
                }
            }
            URL u = new URL(simcraft_versions.get(Collections.max(simcraft_versions.keySet())));
            doc = Jsoup.connect(u.toString()).userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6").referrer("http://www.simulationcraft.org/download.html").get();
            links = doc.select("a[href]");
            for (Element e : links){
                String link = e.attr("abs:href");
                if (link.contains("source")){
                    String[] parse = link.split("/");
                    String fileName = parse[parse.length - 1];
                    File file = new File(new File("/simstuff"), fileName);
                    if (!file.exists()){
                        FileUtils.writeStringToFile(simfile, String.format("echo \"simc_update\" >%s/simc_update.update", www_home).concat(SystemUtils.LINE_SEPARATOR) ,true);
                        FileUtils.writeStringToFile(simfile, String.format("cd %s", simc_dir).concat(SystemUtils.LINE_SEPARATOR) ,true);
                        FileUtils.writeStringToFile(simfile, String.format("rm -rf %s", "simc").concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("git %s", "clone https://code.google.com/p/simulationcraft/").concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("sleep %s", "10").concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("wait").concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("chmod -R 777 %s/simulationcraft", simc_dir).concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("mv %s %s", simc_dir + "/simulationcraft", simc_dir + "/simc", simc_dir).concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("rm -Rf %s", simc_dir + "/simc/.git").concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("zip -r %s %s", simc_dir + "/".concat(fileName), simc_dir + "/simc").concat(SystemUtils.LINE_SEPARATOR), true);
                        FileUtils.writeStringToFile(simfile, String.format("cd %s", simc_dir + "/simc/engine").concat(SystemUtils.LINE_SEPARATOR) ,true);
                        FileUtils.writeStringToFile(simfile, String.format("%s", "make").concat(SystemUtils.LINE_SEPARATOR) ,true);
                        FileUtils.writeStringToFile(simfile, String.format("cd %s", www_home).concat(SystemUtils.LINE_SEPARATOR) ,true);
                        FileUtils.writeStringToFile(simfile, String.format("rm -Rf %s/simulations/*.html", www_home).concat(SystemUtils.LINE_SEPARATOR) ,true);
                        FileUtils.writeStringToFile(simfile, String.format("page=$(curl %s)", page).concat(SystemUtils.LINE_SEPARATOR) ,true);
                        save.delete();
                    }
                }
            }
        }catch(Throwable t){
            t.printStackTrace();
        }
        try{
            if (save.exists()){
                Reader r = new FileReader(save);
                last = json.fromJson(r, HashMap.class);
                r.close();
            }
        }catch(Throwable t){
            t.printStackTrace();
        }
        this.base_region = String.format(base, this.region.toString());
        GuildDao dao = new GuildDao(this.region);
        CharacterDao Char = new CharacterDao(this.region);
        Guild g = null;
        print("Guild name: %s, Realm: %s", this.guildName, this.realm);
        try {
            g = dao.getGuild(this.realm, this.guildName, GuildOptions.ALL);
        } catch (Throwable t) {
            t.printStackTrace();
        }
        if (g == null) {
            print("Failed to find guild: %s", this.guildName);
        }
        for (GuildMember m : g.getMembers()) {
            if (m.getCharacter().getLevel() != MAX_LEVEL){
                continue;
            }
            if (this.custom_sim){
                if (!m.getCharacter().getName().equals(this.custom_name)){
                    continue;
                }
            }
            double bracket = 10 * (Math.floor(Math.abs(m.getCharacter().getLevel() / 10)));
            print("Processing character: %s", m.getCharacter().getName());
            org.halverson.wowapi.entity.Character c = null;
            int tries = 0;
            boolean found = false;
            while (tries <= 10 && !found){
                tries++;
                try {
                    c = Char.getCharacter(this.realm, m.getCharacter().getName(), EnumSet.of(CharacterOptions.BASE, CharacterOptions.ITEMS, CharacterOptions.TALENTS));
                    if (c != null){
                        found = true;
                    }
                } catch (CharacterNotFoundException ex) {
                    print("Character %s not found on Armory. Probably too old.", m.getCharacter().getName());
                    found = false;
                } catch (WowApiException ex2) {
                    print("Armory error!");
                    found = false;
                }
                print("try # %s", tries);
                if (dumpilvl){
                    try{
                        String ilvl = String.format("%s", c.getItems().getAverageItemLevelEquipped());
                        FileUtils.writeStringToFile(new File(new File(ilvldir), c.getName().concat(".txt")), ilvl, false);
                    }catch(Throwable t){
                        t.printStackTrace();
                        found = false;
                    }
                }
                if (dumpClass){
                    try{
                        String ilvl = String.format("%s", c.getCharacterClass().getDescription());
                        FileUtils.writeStringToFile(new File(new File(classDir), c.getName().concat(".txt")), ilvl, false);
                    }catch(Throwable t){
                        t.printStackTrace();
                        found = false;
                    }
                }
            }
            String key = "";
            for (Talents t : c.getTalents()){
                if (t.isSelected()){
                    key += t.toString();
                }
            }
            key += c.getItems().toString();
            String hash = DigestUtils.md5Hex(key);
            if (last.containsKey(c.getName())){
                if (last.get(c.getName()).equals(String.valueOf(hash)) && !this.custom_sim){
                    print("Character not modified. No redraw needed.");
                    continue;
                }
            }
            if (dumpAvatars) {
                this.dumpAvatar(new File(avatarOutput + "/".concat(String.valueOf(((Double) bracket).intValue()))), c);
            }
            print(c.toString());
            String weight = "";
            String simcraft1 = String.format(command, simc_dir, this.region.toString(), this.realm, m.getCharacter().getName(), weight, "1.0", m.getCharacter().getName(), "100");
            String simcraft2 = String.format(command, simc_dir, this.region.toString(), this.realm, m.getCharacter().getName(), weight, "0.9", m.getCharacter().getName(), "90");
            String simcraft3 = String.format(command, simc_dir, this.region.toString(), this.realm, m.getCharacter().getName(), weight, "0.8", m.getCharacter().getName(), "80");
            String simcraft4 = String.format(command, simc_dir, this.region.toString(), this.realm, m.getCharacter().getName(), weight, "0.7", m.getCharacter().getName(), "70");
            String simcraft5 = String.format(command, simc_dir, this.region.toString(), this.realm, m.getCharacter().getName(), "calculate_scale_factors=1", "1.0", m.getCharacter().getName(), "weights");
            int phases = 4;
            if (doWeights){
                phases++;
            }
            try{
                FileUtils.writeStringToFile(simfile, String.format("echo \"%s\" >/var/www/cotd/forums/phases.txt", phases).concat(SystemUtils.LINE_SEPARATOR), true);
            }catch(Throwable t){
                t.printStackTrace();
            }
            if (dosim && !bannedPlayers.contains(m.getCharacter().getName())) {
                this.clearLog();
                this.WriteToLogAndSim(simcraft1, c.getName(), 1);
                this.WriteToLogAndSim(simcraft2, c.getName(), 2);
                this.WriteToLogAndSim(simcraft3, c.getName(), 3);
                this.WriteToLogAndSim(simcraft4, c.getName(), 4);
                if (doWeights){
                    this.WriteToLogAndSim(simcraft5, c.getName(), 5);
                }
            }
            last.put(c.getName(), String.valueOf(hash));
        }
        try{
            Writer w = new FileWriter(save);
            json.toJson(last, HashMap.class, w);
            w.close();
            FileUtils.deleteDirectory(new File(avatarOutput.concat("/temp")));
            if (dosim){
                FileUtils.writeStringToFile(simfile, "rm /simstuff/runSimulations.sh".concat(SystemUtils.LINE_SEPARATOR), true);
            }
        }catch(Throwable t) {
            t.printStackTrace();
            simfile.deleteOnExit();
        }
    }

    private void WriteToLogAndSim(String simcraft1, String name, int pass){
        try{
            FileUtils.writeStringToFile(simfile, String.format("echo %s >%s/simc.stage".concat(SystemUtils.LINE_SEPARATOR), pass, www_home), true);
            FileUtils.writeStringToFile(simfile, String.format("echo %s >%s/simc.current".concat(SystemUtils.LINE_SEPARATOR), name, www_home), true);
            FileUtils.writeStringToFile(simfile, String.format(simcraft1).concat(SystemUtils.LINE_SEPARATOR), true);
            FileUtils.writeStringToFile(simfile, String.format("rm %s/simc.current".concat(SystemUtils.LINE_SEPARATOR), www_home), true);
            FileUtils.writeStringToFile(simfile, String.format("rm %s/simc.stage".concat(SystemUtils.LINE_SEPARATOR), www_home), true);
        }catch(Throwable t){
            t.printStackTrace();
        }

        try{
            FileUtils.writeStringToFile(simLog, String.format("echo %s >%s/simc.stage".concat(SystemUtils.LINE_SEPARATOR), pass, www_home), true);
            FileUtils.writeStringToFile(simLog, String.format("echo %s >%s/simc.current".concat(SystemUtils.LINE_SEPARATOR), name, www_home), true);
            FileUtils.writeStringToFile(simLog, String.format(simcraft1).concat(SystemUtils.LINE_SEPARATOR), true);
            FileUtils.writeStringToFile(simLog, String.format("rm %s/simc.current".concat(SystemUtils.LINE_SEPARATOR), www_home), true);
            FileUtils.writeStringToFile(simLog, String.format("rm %s/simc.stage".concat(SystemUtils.LINE_SEPARATOR), www_home), true);
        }catch(Throwable t){
            t.printStackTrace();
        }
    }

    private void clearLog(){
        try{
            FileUtils.writeStringToFile(simLog, SystemUtils.LINE_SEPARATOR, false);
        }catch(Throwable t){
            t.printStackTrace();
        }
    }

    public static void main(String[] args) {
        instance = new Main(args);
    }

    private static int pickOptimalFontSize(Graphics2D g, String title, int width, int height) {
        Rectangle2D rect = null;

        int fontSize = 30; //initial value
        do {
            fontSize--;
            Font font = new Font("Arial", Font.BOLD, fontSize);
            rect = getStringBoundsRectangle2D(g, title, font);
        } while (rect.getWidth() >= width || rect.getHeight() >= height);

        return fontSize;
    }

    public static Rectangle2D getStringBoundsRectangle2D(Graphics g, String title, Font font) {
        g.setFont(font);
        FontMetrics fm = g.getFontMetrics();
        Rectangle2D rect = fm.getStringBounds(title, g);
        return rect;
    }

    private void dumpAvatar(File output, Character c) {
        try {
            String url = base_region.concat(c.getThumbnail());
            File temp = new File(avatarOutput + "/temp/".concat(c.getName()).concat(".png"));
            FileUtils.copyURLToFile(new URL(url), temp);
            BufferedImage in = ImageIO.read(temp);
            BufferedImage out = this.deepCopy(in);
            Graphics graphics = out.getGraphics();
            Graphics2D d = (Graphics2D) graphics;
            FontRenderContext context = d.getFontRenderContext();
            Font font = new Font("Arial", Font.BOLD, pickOptimalFontSize(d, c.getName(), out.getWidth(), out.getHeight()));
            Rectangle r = new Rectangle();
            r.setSize(out.getWidth() - 3, out.getHeight());
            TextLayout textLayout = new TextLayout(c.getName(), font, context);
            double transX = (out.getWidth() / 2) - textLayout.getBounds().getWidth() / 2;
            float transY = (out.getHeight() / 2) + textLayout.getDescent() + 32;
            AffineTransform transform = new AffineTransform();
            transform.setToTranslation(transX, transY);
            Shape shape = textLayout.getOutline(transform);
            graphics.setColor(Color.white);
            d.fill(shape);
            graphics.setColor(Color.black);
            Stroke stroke = new BasicStroke(1);
            d.setStroke(stroke);
            d.draw(shape);
            output.mkdirs();
            BufferedImage border = new BufferedImage(out.getWidth()+2,out.getHeight()+2,BufferedImage.TYPE_INT_RGB );
            Graphics graphic = border.getGraphics();
            graphic.setColor(Color.black);
            graphic.fillRect(0,0,border.getWidth(),border.getHeight());
            graphic.drawImage(out,1,1,null);
            ImageIO.write(border, "png", new File(output, c.getName().concat(".png")));
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private BufferedImage deepCopy(BufferedImage bi) {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
    }

    private void sleep(int time) {
        try {
            Thread.sleep(time * 1000);
        } catch (Throwable t2) {
            t2.printStackTrace();
        }
    }

    public void run(String command) {
        try {
            print("Running command: %s", command);
            Process p = Runtime.getRuntime().exec(command);
            p.waitFor();
            print("Simcraft ended.");
            sleep(5);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private void print(String msg, Object... params) {
        System.out.println(String.format(msg, params));
    }
}
