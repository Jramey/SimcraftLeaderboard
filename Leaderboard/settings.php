<?php
// Setting this to true disables the board.
$maint = false;

$path = "/var/www/cotd/forums/";
$sim_path = "/simstuff/";
$avatar_path = "http://cotd.inpureprojects.info/images/avatars/gallery/100/";
// For the maintenance message.
$admin_name = "den";

class DBConnect
{
// Example credentials. You should change these to your actual ones for this page.
private $servername = "localhost";
private $username = "simcraft";
private $password = "moardps";
private $dbName = "simc";
private $db;

public function __construct()
{
$this->db = new mysqli($this->servername, $this->username, $this->password);
if ($this->db->connect_error) {
die("Connection failed: " . $this->db->connect_error);
} else {
$this->db->autocommit(true);
if ($this->query("use $this->dbName;") == false) {
die("Failed to open database.");
};
}
}

public function query($query)
{
return $this->db->query($query);
}

public function close()
{
$this->db->close();
}

public function error()
{
$this->db->error;
}
}

$db = new DBConnect();

// Enables/disables display of tables.
$display_tank = true;
$display_dps = true;
$display_healers = true;
$display_ilvl = false;
$display_status = true;

// This displays the failed to get info from bnet error.
$display_bnetdown = false;

$skill = "100";

// Settings things global.
$GLOBALS["bnet"] = $display_bnetdown;
$GLOBALS["sim_path"] = $sim_path;
$GLOBALS["path"] = $path;
$GLOBALS["avatar_path"] = $avatar_path;