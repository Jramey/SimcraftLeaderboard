<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Leaderboard</title>

    <?php

    require 'shared.php';

    head();

    ?>

    <script src="jquery.min.js"></script>
    <script>

        var pageGenID;
        var status_update_delay_in_seconds = 5;
        var update_delay_in_seconds = 10;

        <?php
            $gen = file_get_contents("pagegen.txt");
            echo "pageGenID=parseInt(\"$gen\")";
        ?>

        // 1 = tanks
        // 2 = dps
        // 3 = ilvl
        // 4 = healers

        function setup_flip(flip, panel){
            $(document).ready(function () {
                $(flip).click(function () {
                    $(panel).slideToggle("fast");
                });
            });
        }

        setup_flip("#flip", "#panel");
        setup_flip("#flip2", "#panel2");
        setup_flip("#flip3", "#panel3");
        setup_flip("#flip4", "#panel4");

        function getCurrent(){
            $.get( "pagegen.php", function(data) {
                if (parseInt(data) != pageGenID){
                    refresh_table("#panel");
                    refresh_table("#panel2");
                    refresh_table("#panel3");
                    refresh_table("#panel4");
                    pageGenID = parseInt(data);
                }
            });
        }

        function refresh_table(panel){
            if (jQuery){
                $( panel).fadeOut('slow', function(){
                    $(panel).html('');
                    $(panel ).load("leaderboard.php " + panel);
                    $(panel).fadeIn();
                });
            }else{
                alert("jQuery not loaded!");
            }
        }

        function refresh_status(){
            if (jQuery){
                $( "#statusbox" ).load( "leaderboard.php #statusbox" );
            }else{
                alert("jQuery not loaded!");
            }
        }

        onload = function () {
            window.setInterval(refresh_status, (status_update_delay_in_seconds * 1000));
            window.setInterval(getCurrent, (update_delay_in_seconds * 1000));
        }

    </script>
</head>
<body>
<a href="https://github.com/INpureProjects/SimcraftLeaderboard"><img style="position: absolute; top: 0; right: 0; border: 0;" src="http://s3.amazonaws.com/github/ribbons/forkme_right_darkblue_121621.png" alt="Fork me on GitHub" /></a>
<form name="form1" align="center" method="post">
    Skill Level:
    <?php

    function is_lockdown()
    {
        return file_exists("simc.lock");
    }
	
    function generate_option($skill, $desc){
        $selected = "";
        if (isset($_POST["skill"])){
            if ($_POST["skill"] == $skill){
                $selected = "selected=\"selected\"";
            }
        }
        echo "<option value=\"$skill\" $selected>$desc</option>";
    }

    if (is_lockdown()) {
        echo "<select name=\"skill\" disabled>";
    } else {
        echo "<select name=\"skill\">";
    }

    generate_option("100", "Expert (100%)");
    generate_option("90", "Above-Average (90%)");
    generate_option("80", "Average (80%)");
    generate_option("70", "Ouch, fire is hot! (70%)");

    echo "</select>";

    if (is_lockdown()) {
        echo "<INPUT TYPE = \"Submit\" Name = \"level\" VALUE = \"Change Settings\" disabled>";
    } else {
        echo "<INPUT TYPE = \"Submit\" Name = \"level\" VALUE = \"Change Settings\">";
    }

    ?>

    <a href="simcraft.log">View Log</a>
</form>
</br>
<p align="center">
    <?php

    require 'settings.php';

    function purge($db)
    {
        echo "Purging Database for simc update!<br/>";
        $skillArray = array("100", "90", "80", "70");
        foreach ($skillArray as $skill) {
            $db->query("Truncate table tanks_$skill");
            $db->query("Truncate table tanks_$skill" . "_old");
            $db->query("Truncate table dps_$skill");
            $db->query("Truncate table dps_$skill" . "_old");
            $db->query("Truncate table healers_$skill");
            $db->query("Truncate table healers_$skill" . "_old");
        }
        $db->query("Truncate table lastrun;");
        $db->query("Truncate table tank_weights;");
        $db->query("Truncate table dps_weights;");
        $db->query("Truncate table healer_weights;");
        $db->query("Truncate table characters");
        unlink("simc_update.update");
        $dir = "simulations/*_";
        foreach (glob($dir) as $file) {
            if (is_file($file)) {
                unlink($file);
            }
        }
    }

    if (isset($_POST["purge"])) {
        if ($_POST["purge"] == "true") {
            purge($db);
        }
    }

    if (isset($_POST["skill"])) {
        $skill = $_POST["skill"];
    }
    if (isset($_POST["ilvl"])) {
        if ($_POST["ilvl"] == "Display iLvl Rankings Table") {
            $display_ilvl = true;
            $display_tank = false;
            $display_dps = false;
            $display_healers = false;
        }
    }

    if (isset($_GET["update_db"])) {
        if ($_GET["update_db"] == "true") {
            update_db($db);
        }
    }

    if (isset($_POST["resim_individual"])) {
        $temp = explode(" ", $_POST["resim_individual"]);
        $name = $temp[1];
        echo "Resimming $name.</br>";
    }

    ini_set("display_errors", "1");
    error_reporting(-1);
    mb_language('uni');
    mb_internal_encoding('UTF-8');

    class Tank
    {
        public $name;
        public $DPS;
        public $DTPS;
        public $HPS;
        public $APS;
        public $TMI;
        public $rawScore;
        public $ilvl;
        public $stats;

        public static function fromIndex($index)
        {
            $tank = new Tank();
            $tank->fillFromIndex($index);
            return $tank;
        }

        protected function fillFromIndex($index)
        {
            $split = explode(":", $index);
            $_stats = explode(",", $split[1]);
            $this->name = explode("_", substr($split[0], 0, strlen($split[0]) - 6))[0];
            $this->DPS = remove_label(substr($_stats[0], 6, strlen($_stats[0])), 0);
            $this->DTPS = remove_label($_stats[1], 1);
            $this->HPS = remove_label(explode("(", $_stats[2])[0], 1);
            $this->APS = remove_label(str_replace(")", "", explode("(", $_stats[2])[1]), 0);
            $this->TMI = remove_label($_stats[3], 1);
            $this->ilvl = file_get_contents("ilvl/" . $this->name . ".txt");
            $this->rawScore = (intval($this->DTPS) + (intval($this->TMI)) * 1000) - (intval($this->HPS) + intval($this->APS));
            $this->stats[] = $this->ilvl;
            $this->stats[] = $this->DPS;
            $this->stats[] = $this->DTPS;
            $this->stats[] = $this->HPS;
            $this->stats[] = $this->APS;
            $this->stats[] = $this->TMI;
            $this->stats[] = $this->rawScore;
        }

        public static function fromRow($row)
        {
            $tank = new Tank();
            $tank->fillFromRow($row);
            return $tank;
        }

        protected function fillFromRow($row)
        {
            $this->name = $row["name"];
            $this->ilvl = $row["ilvl"];
            if ($this->ilvl == 0) {
                //echo "Error: Failed to retrieve ilvl for $this->name from battle.net<br/>";
                $GLOBALS["bnet"] = true;
            }
            $this->DPS = $row["dps"];
            $this->DTPS = $row["dtps"];
            $this->HPS = $row["hps"];
            $this->APS = $row["aps"];
            $this->TMI = $row["tmi"];
            $this->rawScore = (intval($this->DTPS) + (intval($this->TMI)) * 1000) - (intval($this->HPS) + intval($this->APS));
            $this->stats[] = $this->ilvl;
            $this->stats[] = $this->DPS;
            $this->stats[] = $this->DTPS;
            $this->stats[] = $this->HPS;
            $this->stats[] = $this->APS;
            $this->stats[] = $this->TMI;
            $this->stats[] = $this->rawScore;
        }
    }

    class DPS
    {
        public $name;
        public $DPS;
        public $ilvl;

        public static function fromIndex($name, $index)
        {
            $dps = new DPS();
            $dps->fillFromIndex($name, $index);
            return $dps;
        }

        protected function fillFromIndex($name, $index)
        {
            $this->name = explode("_", $name)[0];
            $this->DPS = remove_label(str_replace(";", "", str_replace(":&#160", "", $index)), 0);
            $this->ilvl = file_get_contents("ilvl/" . $this->name . ".txt");
        }

        public static function fromRow($row)
        {
            $dps = new DPS();
            $dps->fillFromRow($row);
            return $dps;
        }

        protected function fillFromRow($row)
        {
            $this->name = $row["name"];
            $this->DPS = $row["dps"];
            $this->ilvl = $row["ilvl"];
            if ($this->ilvl == 0) {
                //echo "Error: Failed to retrieve ilvl for $this->name from battle.net<br/>";
                $GLOBALS["bnet"] = true;
            }
        }

    }

    class Healer
    {
        public $name;
        public $HPS;
        public $ilvl;

        public static function fromIndex($name, $index)
        {
            $healer = new Healer();
            $healer->fillFromIndex($name, $index);
            return $healer;
        }

        protected function fillFromIndex($name, $index)
        {
            $this->name = explode("_", $name)[0];
            $this->HPS = str_replace("Raid HPS+APS:", "", $index);
            $this->ilvl = file_get_contents("ilvl/" . $this->name . ".txt");
        }

        public static function fromRow($row)
        {
            $healer = new Healer();
            $healer->fillFromRow($row);
            return $healer;
        }

        protected function fillFromRow($row)
        {
            $this->name = $row["name"];
            $this->HPS = $row["hps"];
            $this->ilvl = $row["ilvl"];
        }
    }

    class Character
    {
        public $name;
        public $ilvl;

        public static function fromRow($row)
        {
            $c = new Character();
            $c->fillFromRow($row);
            return $c;
        }

        protected function fillFromRow($row)
        {
            $this->name = $row["name"];
            $this->ilvl = $row["ilvl"];
            if ($this->ilvl == 0) {
                //echo "Error: Failed to retrieve ilvl for $this->name from battle.net<br/>";
                $GLOBALS["bnet"] = true;
            }
        }
    }

    class Weights
    {
        public static $find = "&gt;";
        public $weight;

        public static function fromRow($row)
        {
            $w = new Weights();
            $w->fillFromRow($row);
            return $w;
        }

        protected function fillFromRow($row)
        {
            $this->weight = $row["stats"];
        }

        public static function fromIndex($index)
        {
            $w = new Weights();
            $w->fillFromIndex($index);
            return $w;
        }

        protected function fillFromIndex($index)
        {
            $this->weight = $index;
        }
    }

    function quick_sim($name)
    {

    }

    function findRole($html)
    {
        return getRole($html);
    }

    class Roles{
        static $tank = 0;
        static $dps = 1;
        static $healer = 2;

        static function isTank($index){
            return Roles::$tank == $index;
        }

        static function isDps($index){
            return Roles::$dps == $index;
        }

        static function isHealer($index){
            return Roles::$healer == $index;
        }

    }

    function getRole($html)
    {
        $tanks = array("Role: Tank");
        $dps = array("Role: Spell", "Role: Attack", "Role: Dps");
        $healers = array("Role: Heal");
        foreach ($tanks as $tank) {
            if (_getRole($html, $tank)) {
                return Roles::$tank;
            }
        }
        foreach ($dps as $deeps) {
            if (_getRole($html, $deeps)) {
                return Roles::$dps;
            }
        }
        foreach ($healers as $healer) {
            if (_getRole($html, $healer)) {
                return Roles::$healer;
            }
        }
    }

    function _getRole($html, $role)
    {
        $find = strpos($html, $role);
        $sub = substr($find, 0, 2);
        if ($sub == 21) {
            return true;
        } else {
            return false;
        }
    }

    function update_db($db)
    {
        $skillArray = array("100", "90", "80", "70");
        $db->query("replace into characters_old select * from characters");
        foreach ($skillArray as $skill) {
            $dir = "simulations/*_" . $skill . ".html";
            foreach (glob($dir) as $file) {
                $entry = str_replace(".html", "", str_replace("simulations/", "", $file));
                $html = strip_tags(file_get_contents($file));
                $parse = explode(PHP_EOL, $html);
                if (Roles::isTank(findRole($html))) {
                    foreach ($parse as $i) {
                        $f = strstr($i, "dtps");
                        if ($f != NULL) {
                            $tank = Tank::fromIndex($i);
                            $result = $db->query("select * from lastrun where name='$tank->name'");
                            $time = filemtime($file);
                            while ($row = $result->fetch_assoc()) {
                                if ($row["time"] != $time) {
                                    $db->query("replace into tanks_" . $skill . "_old select * from tanks_" . $skill . " where name='$tank->name'");
                                }
                            }
                            $db->query("INSERT INTO lastrun (name, time) VALUES ('$tank->name', '$time') ON DUPLICATE KEY UPDATE time='$time'");
                            $db->query("INSERT INTO characters (name, ilvl) VALUES ('$tank->name', '$tank->ilvl') ON DUPLICATE KEY UPDATE ilvl='$tank->ilvl'");
                            $db->query("INSERT INTO tanks_$skill (name, dps, dtps, hps, aps, tmi, ilvl) VALUES('$tank->name', '$tank->DPS', '$tank->DTPS', '$tank->HPS', '$tank->APS', '$tank->TMI', '$tank->ilvl') ON DUPLICATE KEY UPDATE dps='$tank->DPS', dtps='$tank->DTPS', aps='$tank->APS', hps='$tank->HPS', tmi='$tank->TMI', ilvl='$tank->ilvl'");

                        }
                    }
                } else if (Roles::isDps(findRole($html))) {
                    foreach ($parse as $i) {
                        $f = strstr($i, ":&#160");
                        if ($f != NULL) {
                            $dps = DPS::fromIndex($entry, $f);
                            $result = $db->query("select * from lastrun where name='$dps->name'");
                            $time = filemtime($file);
                            while ($row = $result->fetch_assoc()) {
                                if ($row["time"] != $time) {
                                    $db->query("replace into dps_" . $skill . "_old select * from dps_" . $skill . " where name='$dps->name'");
                                }
                            }
                            $db->query("INSERT INTO lastrun (name, time) VALUES ('$dps->name', '$time') ON DUPLICATE KEY UPDATE time='$time'");
                            $db->query("INSERT INTO characters (name, ilvl) VALUES ('$dps->name', '$dps->ilvl') ON DUPLICATE KEY UPDATE ilvl='$dps->ilvl'");
                            $db->query("INSERT INTO dps_$skill (name, dps, ilvl) VALUES ('$dps->name', '$dps->DPS', '$dps->ilvl') ON DUPLICATE KEY UPDATE dps='$dps->DPS', ilvl='$dps->ilvl'");
                            break;
                        }
                    }
                } else if (Roles::isHealer(findRole($html))) {
                    foreach ($parse as $i) {
                        $f = strstr($i, "Raid HPS+APS:");
                        if ($f != NULL) {
                            $healer = Healer::fromIndex($entry, $f);
                            $result = $db->query("select * from lastrun where name='$healer->name'");
                            $time = filemtime($file);
                            while ($row = $result->fetch_assoc()) {
                                if ($row["time"] != $time) {
                                    $db->query("replace into healers_" . $skill . "_old select * from healers_" . $skill . " where name='$healer->name'");
                                }
                            }
                            $db->query("INSERT INTO lastrun (name, time) VALUES ('$healer->name', '$time') ON DUPLICATE KEY UPDATE time='$time'");
                            $db->query("INSERT INTO characters (name, ilvl) VALUES ('$healer->name', '$healer->ilvl') ON DUPLICATE KEY UPDATE ilvl='$healer->ilvl'");
                            $db->query("INSERT INTO healers_$skill (name, hps, ilvl) VALUES ('$healer->name', '$healer->HPS', '$healer->ilvl') ON DUPLICATE KEY UPDATE hps='$healer->HPS', ilvl='$healer->ilvl'");
                            break;
                        }
                    }
                }
            }
        }
        $dir = "simulations/*_weights.html";
        foreach (glob($dir) as $file) {
            $entry = explode("_", str_replace(".html", "", str_replace("simulations/", "", $file)))[0];
            $html = strip_tags(file_get_contents($file));
            $table = NULL;
            if (Roles::isTank(findRole($html))) {
                $table = "tank_weights";
            } else if (Roles::isDps(findRole($html))) {
                $table = "dps_weights";
            } else if (Roles::isHealer(findRole($html))) {
                $table = "healer_weights";
            }
            $parse = explode(PHP_EOL, $html);
            foreach ($parse as $i) {
                if (strpos($i, ">") !== false) {
                    if (substr_count($i, ">") > 1) {
                        $w = Weights::fromIndex($i);
                        $db->query("INSERT INTO $table (name, stats) VALUES ('$entry', '$w->weight') ON DUPLICATE KEY UPDATE stats='$w->weight'");
                        break;
                    }
                }
            }
        }
        $myfile = fopen("pagegen.txt", "w");
        $timestamp = time();
        fwrite($myfile, $timestamp);
        fclose($myfile);
        unlink("simc.update");
    }

    function remove_label($tab, $index)
    {
        $e = explode(" ", $tab);
        return $e[$index];
    }

    function sort_tanks($tank1, $tank2)
    {
        return $tank1->rawScore > $tank2->rawScore;
    }

    function sort_dps($dps1, $dps2)
    {
        return $dps1->DPS < $dps2->DPS;
    }

    function sort_ilvl($c1, $c2)
    {
        return $c1->ilvl < $c2->ilvl;
    }

    function sort_healers($h1, $h2)
    {
        return $h1->HPS < $h2->HPS;
    }

    function link_sim($name, $skill){
        echo "<a href=\"simulations/$name" . "_" . $skill . ".html\">" . $name . "</a>";
    }

    function tank_table($skill, $db)
    {
        echo "<div align=\"center\" id=\"flip\"><strong>Tank Leaderboard</strong></div>";
        echo "<div align=\"center\" id=\"panel\">";
        echo "<table border=\"3\" align=\"center\">";
        echo "<tr>";
        echo "<td>Rank</td>";
        echo "<td>Player</td>";
        echo "<td>ilvl</td>"; // 0
        echo "<td>DPS</td>"; // 1
        echo "<td>DTPS</td>"; // 2
        echo "<td>HPS</td>"; // 3
        echo "<td>APS</td>"; // 4
        echo "<td><a href=\"http://www.sacredduty.net/theck-meloree-index-standard-reference-document/\">TMI</a></td>"; // 5
        echo "<td>Total Score</td>"; // 6
        echo "<td>Previous Score</td>";
        echo "<td>Weights</td>";
        echo "<td>Options</td>";
        echo "</tr>";
        $count = 0;
        $tankArray = array();
        $q = $db->query("SELECT * FROM tanks_$skill");
        if ($q != NULL) {
            while ($row = $q->fetch_assoc()) {
                $index = Tank::fromRow($row);
                $tankArray[] = $index;
            }
        }
        usort($tankArray, "sort_tanks");
        foreach ($tankArray as $index) {
            $count++;
            echo "<tr>";
            echo "<td>" . "#" . $count . "</td>";
            echo "<td>";
            link_sim($index->name, $skill);
            echo "</td>";
            foreach ($index->stats as $stat) {
                echo "<td>";
                if (strpos($stat, "k")) {
                    echo $stat;
                } else {
                    echo number_format(intval($stat));
                }
                echo "</td>";
            }
            $q = $db->query("SELECT * FROM tanks_$skill" . "_old" . " where name='$index->name'");
            $tankold = NULL;
            echo "<td>";
            while ($row = $q->fetch_assoc()) {
                $tankold = Tank::fromRow($row);
                $oldScore = number_format(intval($tankold->rawScore));
                echo "$oldScore";
            }
            if ($tankold == NULL) {
                echo "No data.";
            }
            echo "</td>";
            echo "<td>";
            $q = $db->query("SELECT * FROM tank_weights where name='$index->name'");
            $w = NULL;
            while ($row = $q->fetch_assoc()) {
                $w = Weights::fromRow($row);
                echo "$w->weight";
            }
            if ($w == NULL) {
                echo "No Data.";
            }
            echo "</td>";
            echo "<td>";
            echo "<form name=\"form$index->name\" method=\"post\" >";
            if (is_lockdown()) {
                echo "<button type=\"submit\" name=\"doSingle\" value=\"$index->name\" disabled>Update</button>";
                echo "<button type=\"submit\" name=\"doWeights\" value=\"$index->name\" disabled>Recalculate Weights</button>";
            } else {
                echo "<button type=\"submit\" name=\"doSingle\" value=\"$index->name\">Update</button>";
                echo "<button type=\"submit\" name=\"doWeights\" value=\"$index->name\">Recalculate Weights</button>";
            }
            echo "</form>";
            echo "</td>";
            echo "</tr>";
        }
        echo "</table></div><br/><br/>";
    }

    function dps_table($skill, $db, $boardLength)
    {
        // DPS
        echo "<div align=\"center\" id=\"flip2\"><strong>DPS Leaderboard</strong></div>";
        $count = 0;
        echo "<div align=\"center\" id=\"panel2\">";
        echo "<table border=\"3\" align=\"center\">";
        echo "<tr>";
        echo "<td>Rank</td>";
        echo "<td>Player</td>";
        echo "<td>ilvl</td>";
        echo "<td>DPS</td>";
        echo "<td>Previous DPS</td>";
        echo "<td>Weights</td>";
        echo "<td>Options</td>";
        echo "</tr>";
        $q = $db->query("SELECT * FROM dps_$skill");
        if ($q != NULL) {
            $dpsArray = array();
            while ($row = $q->fetch_assoc()) {
                $index = DPS::fromRow($row);
                $dpsArray[] = $index;
            }
        }
        usort($dpsArray, "sort_dps");
        foreach ($dpsArray as $index) {
            $count++;
            if ($count > $boardLength) {
                break;
            }
            echo "<tr>";
            echo "<td>" . "#" . $count . "</td>";
            echo "<td>";
            link_sim($index->name, $skill);
            echo "</td>";
            echo "<td>" . number_format(intval($index->ilvl)) . "</td>";
            echo "<td>" . number_format(intval($index->DPS)) . "</td>";
            $q = $db->query("SELECT * FROM dps_$skill" . "_old" . " where name='$index->name'");
            $dpsold = NULL;
            echo "<td>";
            while ($row = $q->fetch_assoc()) {
                $dpsold = DPS::fromRow($row);
                $oldScore = number_format(intval($dpsold->DPS));
                echo "$oldScore";
            }
            if ($dpsold == NULL) {
                echo "No data.";
            }
            echo "</td>";
            echo "<td>";
            $q = $db->query("SELECT * FROM dps_weights where name='$index->name'");
            $w = NULL;
            while ($row = $q->fetch_assoc()) {
                $w = Weights::fromRow($row);
                echo "$w->weight";
            }
            if ($w == NULL) {
                echo "No Data.";
            }
            echo "</td>";
            echo "<td>";
            echo "<form name=\"form$index->name\" method=\"post\" >";
            if (is_lockdown()) {
                echo "<button type=\"submit\" name=\"doSingle\" value=\"$index->name\" disabled>Update</button>";
                echo "<button type=\"submit\" name=\"doWeights\" value=\"$index->name\" disabled>Recalculate Weights</button>";
            } else {
                echo "<button type=\"submit\" name=\"doSingle\" value=\"$index->name\">Update</button>";
                echo "<button type=\"submit\" name=\"doWeights\" value=\"$index->name\">Recalculate Weights</button>";
            }
            echo "</form>";
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo "</div>";
        if ($count > $boardLength) {
            echo "<div align=\"center\">";
            echo "<form name=\"form69\" align=\"center\" method=\"post\" >";
            echo "<button type=\"submit\" name=\"view_all_dps\" value=\"true\">See Full Rankings</button><br/>";
            echo "</form>";
            echo "</div><br/>";
        }
        if (isset($_POST["view_all_dps"])){
            if ($_POST["view_all_dps"] == "true"){
                echo "<div align=\"center\">";
                echo "<form name=\"form70\" align=\"center\" method=\"post\" >";
                echo "<button type=\"submit\" name=\"view_all_dps\" value=\"false\">Back to leaderboard.</button><br/>";
                echo "</form>";
                echo "</div><br/>";
            }
        }
    }

    function ilvl_table($skill, $db)
    {
        echo "<div align=\"center\" id=\"flip3\"><strong>ilvl Leaderboard</strong></div>";
        $count = 0;
        echo "<div align=\"center\" id=\"panel3\">";
        echo "<table border=\"3\" align=\"center\">";
        echo "<tr>";
        echo "<td>Rank</td>";
        echo "<td>Player</td>";
        echo "<td>ilvl</td>";
        echo "</tr>";
        $ilvlArray = array();
        $q = $db->query("SELECT * FROM characters");
        if ($q != NULL) {
            while ($row = $q->fetch_assoc()) {
                $index = Character::fromRow($row);
                $ilvlArray[] = $index;
            }
            usort($ilvlArray, "sort_ilvl");
            $lastilvl = 0;
            foreach ($ilvlArray as $index) {
                if ($lastilvl != intval($index->ilvl)) {
                    $count++;
                }
                $lastilvl = intval($index->ilvl);
                echo "<tr>";
                echo "<td>" . "#" . $count . "</td>";
                echo "<td>";
                link_sim($index->name, $skill);
                echo "</td>";
                echo "<td>" . number_format(intval($index->ilvl)) . "</td>";
                echo "</tr>";
            }
        }
        echo "</table>";
        echo "</div>";
        echo "<form name=\"form3\" align=\"center\" method=\"post\" >";
        echo "<INPUT TYPE = \"Submit\" Name = \"ilvl\" VALUE = \"Back to the normal leaderboard\">";
        echo "</form><br/>";
    }

    function loadloop()
    {
        echo "<img src=\"Loading-Loop.gif\"";
    }

    function readLastLine($file)
    {
        $fp = @fopen($file, "r");

        $pos = -1;
        $t = " ";
        while ($t != "\n") {
            if (!fseek($fp, $pos, SEEK_END)) {
                $t = fgetc($fp);
                $pos = $pos - 1;
            } else {
                rewind($fp);
                break;
            }
        }
        $t = fgets($fp);
        fclose($fp);
        return $t;
    }

    function healer_table($skill, $db)
    {
        echo "<div align=\"center\" id=\"flip4\"><strong>Healer Leaderboard</strong></div>";
        $count = 0;
        echo "<div align=\"center\" id=\"panel4\">";
        echo "<table border=\"3\" align=\"center\">";
        echo "<tr>";
        echo "<td>Rank</td>";
        echo "<td>Player</td>";
        echo "<td>ilvl</td>";
        echo "<td>HPS+APS</td>";
        echo "<td>Previous HPS+APS</td>";
        echo "<td>Weights</td>";
        echo "<td>Options</td>";
        echo "</tr>";
        $q = $db->query("SELECT * FROM healers_$skill");
        if ($q != NULL) {
            $healerArray = array();
            while ($row = $q->fetch_assoc()) {
                $index = Healer::fromRow($row);
                $healerArray[] = $index;
            }
        }
        usort($healerArray, "sort_healers");
        foreach ($healerArray as $index) {
            $count++;
            echo "<tr>";
            echo "<td>" . "#" . $count . "</td>";
            echo "<td>";
            link_sim($index->name, $skill);
            echo "</td>";
            echo "<td>" . number_format(intval($index->ilvl)) . "</td>";
            echo "<td>" . number_format(intval($index->HPS)) . "</td>";
            $q = $db->query("SELECT * FROM healers_$skill" . "_old" . " where name='$index->name'");
            echo "<td>";
            $hpsold = NULL;
            while ($row = $q->fetch_assoc()) {
                $hpsold = Healer::fromRow($row);
                $oldScore = number_format(intval($hpsold->HPS));
                echo "$oldScore";
            }
            if ($hpsold == NULL) {
                echo "No data.";
            }
            echo "</td>";
            echo "<td>";
            $q = $db->query("SELECT * FROM healer_weights where name='$index->name'");
            $w = NULL;
            while ($row = $q->fetch_assoc()) {
                $w = Weights::fromRow($row);
                echo "$w->weight";
            }

            if ($w == NULL) {
                echo "No Data.";
            }

            echo "</td>";
            echo "<td>";
            echo "<form name=\"form$index->name\" method=\"post\" >";
            if (is_lockdown()) {
                echo "<button type=\"submit\" name=\"doSingle\" value=\"$index->name\" disabled>Update</button>";
                echo "<button type=\"submit\" name=\"doWeights\" value=\"$index->name\" disabled>Recalculate Weights</button>";
            } else {
                echo "<button type=\"submit\" name=\"doSingle\" value=\"$index->name\">Update</button>";
                echo "<button type=\"submit\" name=\"doWeights\" value=\"$index->name\">Recalculate Weights</button>";
            }
            echo "</form>";
            echo "</td>";
            echo "</tr>";
        }
        echo "</table>";
        echo "</div>";
    }


    function ilvl_option()
    {
        echo "<form name=\"form2\" align=\"center\" method=\"post\" >";
        if (is_lockdown()) {
            echo "<INPUT TYPE = \"Submit\" Name = \"ilvl\" VALUE = \"Display iLvl Rankings Table\" disabled>";
        } else {
            echo "<INPUT TYPE = \"Submit\" Name = \"ilvl\" VALUE = \"Display iLvl Rankings Table\">";
        }
        echo "</form><br/>";
    }

    function request_button()
    {
        if (is_lockdown()) {
            echo "<form name=\"form4\" align=\"center\" method=\"post\" >";
            echo "<button type=\"submit\" name=\"resim_all\" value=\"true\" disabled>Request Full Leaderboard Update (No Weights)</button><br/>";
            echo "</form>";
        } else {
            if (file_exists("simc.update")) {
            } else {
                echo "<form name=\"form4\" align=\"center\" method=\"post\" >";
                echo "<button type=\"submit\" name=\"resim_all\" value=\"true\">Request Full Leaderboard Update (No Weights)</button><br/>";
                echo "</form>";
            }
        }
    }

    function bnetdown()
    {
        echo "<div align=\"center\">ilvl colums showing 0 is a failure to retrieve data from battle.net. Please request another simulation later.</div></br>";
    }

    function status(){
        echo "<p align=\"center\">";
        if (isset($_POST["doWeights"])) {
            $char = $_POST["doWeights"];
            echo "Leaderboard update requested!</br>";
            putenv('LANG=en_US.UTF-8');
            if (is_lockdown() == false) {
                exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $GLOBALS["sim_path"]."simcraft_unix_weights.sh $char", $GLOBALS["path"]."simcraft.log", $GLOBALS["sim_path"]."process.pid"));
            } else {
                echo "Simulation already in progress! Please try again later.</br>";
            }
        }
        if (isset($_POST["doSingle"])) {
            $char = $_POST["doSingle"];
            echo "Leaderboard update requested!</br>";
            putenv('LANG=en_US.UTF-8');
            if (is_lockdown() == false) {
                exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $GLOBALS["sim_path"]."simcraft_unix_single.sh $char", $GLOBALS["path"]."simcraft.log", $GLOBALS["sim_path"]."process.pid"));
            } else {
                echo "Simulation already in progress! Please try again later.</br>";
            }
        }
        if (isset($_POST["resim_all"])) {
            if ($_POST["resim_all"] == "true") {
                if (is_lockdown()) {
                    echo "Simulation already in progress! Please try again later.</br>";
                } else {
                    echo "Leaderboard update requested!</br>";
                    putenv('LANG=en_US.UTF-8');
                    exec(sprintf("%s > %s 2>&1 & echo $! >> %s", $GLOBALS["sim_path"]."simcraft_unix.sh", $GLOBALS["path"]."simcraft.log", $GLOBALS["sim_path"]."process.pid"));
                }
            }
        }
        echo "<table border=\"3\" align=\"center\"><tr><td>Server Status</td></tr>";
        echo "<tr><td>";
        if (file_exists("simc_update.update")) {
            echo "Compiling new version of simc...";
            loadloop();
        } else {
            if (file_exists("simc.update")) {
                echo "Updating database...";
                loadloop();
            } else {
                if (is_lockdown()) {
                    if (file_exists("simc.current")) {
                        $phases = file_get_contents("phases.txt");
                        $current = file_get_contents("simc.current");
                        $file = "simcraft.log";
                        $line = readLastLine($file);
                        $lines = explode("Generating ", $line);
                        if (isset($lines[1])) {
                            $test = explode(":", $lines[1])[0];
                        } else {
                            $test = "Thinking";
                        }
                        $line = $lines[count($lines) - 1];
                        $line = substr($line, 0, 34 - 8);
                        $line = str_replace("[", "", $line);
                        $line = str_replace("]", "", $line);
                        $c = substr_count($line, "=");
                        $total = 20;
                        $per = (intval($c) / intval($total)) * 100;
                        $stage = file_get_contents("simc.stage");
                        echo "Simulating:<br/> <a href=\"http://us.battle.net/wow/en/character/hyjal/$current/simple\"><img src=\"".$GLOBALS["avatar_path"].$current.".png\"/></a>";
                        echo "</br>$test: $per%.<br/> Stage $stage of $phases.";
                    } else {
                        echo "Communicating with Battle.net...";
                        loadloop();
                    }
                } else {
                    echo "No simulation running at this time.";
                }
            }
        }
        echo "</td></tr>";
        echo "</table>";
        echo "</p>";
    }

    if ($maint == false) {
        if ($display_tank == true) {
            tank_table($skill, $db);
        }
        if ($display_dps == true) {
            if (isset($_POST["view_all_dps"])){
                if ($_POST["view_all_dps"] == "true"){
                    dps_table($skill, $db, 999);
                }else{
                    dps_table($skill, $db, 10);
                }
            }else{
                dps_table($skill, $db, 10);
            }
        }
        if ($display_healers == true) {
            healer_table($skill, $db);
        }
        if ($display_ilvl == true) {
            ilvl_table($skill, $db);
        } else {
            ilvl_option();
        }
        if ($GLOBALS["bnet"] == true) {
            bnetdown();
        }
        request_button();
        //custom_simulation_button($display_custom);
        if ($display_status == true) {
            echo "<div id=\"statusbox\">";
            status();
            echo "</div>";
        }
    } else {
        echo "$admin_name is currently working on the leaderboard. Come back later!";
    }

    echo $db->error();
    $db->close();

    function powered_by(){
        echo "<div align=\"center\" id=\"power\">";
        echo "Powered by <a href=\"https://github.com/INpureProjects\">INpureProjects</a><br/>";
        echo "</div>";
    }

    powered_by();

    ?>
</p>
</body>
</html>