-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: simc
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `characters`
--

DROP TABLE IF EXISTS `characters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `characters` (
  `name` varchar(12) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `characters_old`
--

DROP TABLE IF EXISTS `characters_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `characters_old` (
  `name` varchar(12) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_100`
--

DROP TABLE IF EXISTS `dps_100`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_100` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_100_old`
--

DROP TABLE IF EXISTS `dps_100_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_100_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_70`
--

DROP TABLE IF EXISTS `dps_70`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_70` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_70_old`
--

DROP TABLE IF EXISTS `dps_70_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_70_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_80`
--

DROP TABLE IF EXISTS `dps_80`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_80` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_80_old`
--

DROP TABLE IF EXISTS `dps_80_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_80_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_90`
--

DROP TABLE IF EXISTS `dps_90`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_90` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_90_old`
--

DROP TABLE IF EXISTS `dps_90_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_90_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dps_weights`
--

DROP TABLE IF EXISTS `dps_weights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dps_weights` (
  `name` varchar(12) NOT NULL,
  `stats` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healer_weights`
--

DROP TABLE IF EXISTS `healer_weights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healer_weights` (
  `name` varchar(12) NOT NULL,
  `stats` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_100`
--

DROP TABLE IF EXISTS `healers_100`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_100` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_100_old`
--

DROP TABLE IF EXISTS `healers_100_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_100_old` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_70`
--

DROP TABLE IF EXISTS `healers_70`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_70` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_70_old`
--

DROP TABLE IF EXISTS `healers_70_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_70_old` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_80`
--

DROP TABLE IF EXISTS `healers_80`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_80` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_80_old`
--

DROP TABLE IF EXISTS `healers_80_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_80_old` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_90`
--

DROP TABLE IF EXISTS `healers_90`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_90` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `healers_90_old`
--

DROP TABLE IF EXISTS `healers_90_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `healers_90_old` (
  `name` varchar(12) NOT NULL,
  `hps` int(11) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lastrun`
--

DROP TABLE IF EXISTS `lastrun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lastrun` (
  `name` varchar(12) NOT NULL,
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tank_weights`
--

DROP TABLE IF EXISTS `tank_weights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tank_weights` (
  `name` varchar(12) NOT NULL,
  `stats` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_100`
--

DROP TABLE IF EXISTS `tanks_100`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_100` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_100_old`
--

DROP TABLE IF EXISTS `tanks_100_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_100_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_70`
--

DROP TABLE IF EXISTS `tanks_70`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_70` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_70_old`
--

DROP TABLE IF EXISTS `tanks_70_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_70_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_80`
--

DROP TABLE IF EXISTS `tanks_80`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_80` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_80_old`
--

DROP TABLE IF EXISTS `tanks_80_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_80_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_90`
--

DROP TABLE IF EXISTS `tanks_90`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_90` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tanks_90_old`
--

DROP TABLE IF EXISTS `tanks_90_old`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tanks_90_old` (
  `name` varchar(12) DEFAULT NULL,
  `dps` int(11) DEFAULT NULL,
  `dtps` int(11) DEFAULT NULL,
  `hps` int(11) DEFAULT NULL,
  `aps` int(11) DEFAULT NULL,
  `tmi` varchar(255) DEFAULT NULL,
  `ilvl` int(11) DEFAULT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-12-08 15:55:49
