This is a leaderboard for World of Warcraft guild characters powered by Simulationcraft.

This was built for use on Linux (Ubuntu specifically).

INSTALL INSTRUCTIONS:

0 Make the sh files from this repo executable with chmod.

1 Install the LAMP stack if you haven't already. Run Setup_Apache.sh to do this automatically.

2 CD to the directory with the files from this repo in it.

3 Import MYSQL/schema.sql into a new database.

4 Run Setup_Simc_Requirements.sh. This will install everything necessary to compile and run Simcraft.

5 Pick a location for your simcraft folder. Copy the contents of the Shell folder to here.

6 Copy SimcraftBatch/out/artifacts/SimcraftBatch_jar/SimcraftBatch.jar to your simcraft folder from #5.

7 Copy the contents of the Leaderboard folder to where you want it to live in www.

8 Edit your copy of settings.php and settings.sh. Set your settings correctly.

9 Check your permissions. The www-data group needs to be an owner of your simcraft folder.

10 Clean your sh files with dos2unix. This is includes in Ubuntu as the command "fromdos".

11 Open your new leaderboard. Hit the button labeled 'request full leaderboard update (no weights)' to begin importing your guild. If you have many level 100s this might take a while.

12 Report any issues you encounter to https://github.com/INpureProjects/SimcraftLeaderboard/issues

FAQ:

Q: How do I set a non-us region?

A: Set your region code in settings.sh